import 'package:blogclub/gen/assets.gen.dart';
import 'package:blogclub/introScreen.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 2)).then((value) => {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => const IntroScreen()))
        });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
              child: Assets.images.background.splash.image(fit: BoxFit.cover)),
          Center(
            child: Assets.images.icons.logo.svg(width: 100),
          )
        ],
      ),
    );
  }
}
