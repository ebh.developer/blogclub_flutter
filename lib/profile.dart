import 'package:blogclub/data.dart';
import 'package:blogclub/gen/assets.gen.dart';
import 'package:blogclub/home.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    final posts = AppDatabase.posts;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: themeData.colorScheme.background.withOpacity(0),
        title: const Text('profile'),
        actions: [
          IconButton(
              onPressed: () {}, icon: const Icon(Icons.more_horiz_rounded)),
          const SizedBox(
            width: 16,
          )
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(children: [
          Stack(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(32, 0, 32, 64),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: themeData.colorScheme.surface,
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 20,
                          color: themeData.colorScheme.onBackground
                              .withOpacity(0.1))
                    ]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(24),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: Assets.images.stories.story8
                                .image(width: 84, height: 84),
                          ),
                          const SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text('@Ebh_Developer'),
                                const SizedBox(
                                  height: 4,
                                ),
                                Text(
                                  'Ebrahim Hamidi',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .copyWith(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 15),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'Flutter Dev',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .apply(
                                          color: themeData.colorScheme.primary),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32, 0, 32, 0),
                      child: Text('About Me',
                          style: themeData.textTheme.titleLarge),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32, 4, 32, 32),
                      child: Text(
                        'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,',
                        style: themeData.textTheme.bodyLarge!
                            .copyWith(fontWeight: FontWeight.w200),
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    )
                  ],
                ),
              ),
              Positioned(
                  bottom: 32,
                  left: 96,
                  right: 96,
                  child: Container(
                    height: 32,
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          blurRadius: 30,
                          color: themeData.colorScheme.onBackground
                              .withOpacity(0.8))
                    ]),
                  )),
              Positioned(
                bottom: 32,
                right: 64,
                left: 64,
                child: Container(
                  height: 68,
                  decoration: BoxDecoration(
                      color: themeData.colorScheme.primary,
                      borderRadius: BorderRadius.circular(12)),
                  child: Row(
                    children: [
                      _UserInformation(
                        themeData: themeData,
                        title: 'Post',
                        count: '52',
                        isHighlighted: true,
                      ),
                      _UserInformation(
                        themeData: themeData,
                        title: 'Following',
                        count: '250',
                        isHighlighted: false,
                      ),
                      _UserInformation(
                        themeData: themeData,
                        title: 'Followers',
                        count: '4.5K',
                        isHighlighted: false,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Container(
            decoration: BoxDecoration(
              color: themeData.colorScheme.surface,
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(32),
                topLeft: Radius.circular(32),
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(32, 16, 32, 16),
                  child: Row(
                    children: [
                      const Expanded(
                        child: Text('My Posts'),
                      ),
                      IconButton(
                          onPressed: () {},
                          icon: Assets.images.icons.grid.svg()),
                      IconButton(
                          onPressed: () {},
                          icon: Assets.images.icons.table.svg()),
                    ],
                  ),
                ),
                for (var i = 0; i < posts.length; i++) PostItems(post: posts[i])
              ],
            ),
          )
        ]),
      ),
    );
  }
}

class _UserInformation extends StatelessWidget {
  const _UserInformation({
    super.key,
    required this.themeData,
    required this.title,
    required this.count,
    required this.isHighlighted,
  });

  final ThemeData themeData;
  final String title;
  final String count;
  final bool isHighlighted;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        decoration: isHighlighted
            ? BoxDecoration(
                color: const Color(0xff2151CD),
                borderRadius: BorderRadius.circular(12))
            : null,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              count,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: themeData.colorScheme.onPrimary),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              title,
              style: themeData.textTheme.bodyLarge!.copyWith(
                  color: themeData.colorScheme.onPrimary,
                  fontWeight: FontWeight.w200),
            )
          ],
        ),
      ),
    );
  }
}
