import 'package:blogclub/gen/assets.gen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ArticleScreen extends StatelessWidget {
  const ArticleScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    return Scaffold(
      floatingActionButton: Container(
        width: 111,
        height: 48,
        decoration: BoxDecoration(
            color: themeData.colorScheme.primary,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                  blurRadius: 20,
                  color: themeData.colorScheme.primary.withOpacity(0.5))
            ]),
        child: InkWell(
          onTap: () {
            showSnackBar(context, 'Like button is clicked');
          },
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Assets.images.icons.thumbs.svg(),
            const SizedBox(
              width: 8,
            ),
            Text(
              '2.1k',
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: themeData.colorScheme.onPrimary),
            ),
          ]),
        ),
      ),
      backgroundColor: themeData.colorScheme.surface,
      body: SafeArea(
          child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverAppBar(
                pinned: true,
                floating: true,
                title: const Text('Article'),
                actions: [
                  IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.more_horiz_rounded)),
                  const SizedBox(
                    width: 16,
                  )
                ],
              ),
              SliverList(
                delegate: SliverChildListDelegate.fixed(
                  [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32, 16, 32, 16),
                      child: Text(
                        'Four Things Every Woman Needs to Know',
                        style: themeData.textTheme.headlineMedium,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32, 0, 16, 32),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: Assets.images.stories.story10.image(
                              width: 48,
                              height: 48,
                              fit: BoxFit.cover,
                            ),
                          ),
                          const SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Ebrahim Hamidi',
                                  style: themeData.textTheme.bodyLarge!
                                      .copyWith(fontWeight: FontWeight.w400),
                                ),
                                const SizedBox(
                                  height: 2,
                                ),
                                const Text('3m ago'),
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              showSnackBar(context, 'Share button is clicked');
                            },
                            icon: const Icon(
                              CupertinoIcons.share,
                            ),
                            color: themeData.colorScheme.primary,
                          ),
                          IconButton(
                            onPressed: () {
                              showSnackBar(
                                  context, 'Bookmark button is clicked');
                            },
                            icon: Icon(
                              CupertinoIcons.bookmark,
                              color: themeData.colorScheme.primary,
                            ),
                          ),
                        ],
                      ),
                    ),
                    ClipRRect(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(32),
                            topRight: Radius.circular(32)),
                        child: Assets.images.background.singlePost.image()),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32, 32, 32, 16),
                      child: Text(
                        'Lorem ipsum is placeholder text commonly used in the graphic',
                        style: themeData.textTheme.headlineSmall,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(32, 0, 32, 32),
                      child: Text(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tempus imperdiet nulla malesuada pellentesque elit eget gravida cum sociis. Tortor consequat id porta nibh. Porta non pulvinar neque laoreet suspendisse. Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Ut venenatis tellus in metus. Sollicitudin nibh sit amet commodo nulla facilisi nullam. Enim lobortis scelerisque fermentum dui faucibus. Velit egestas dui id ornare arcu odio ut sem nulla. Mollis aliquam ut porttitor leo a. Tellus cras adipiscing enim eu turpis. Tristique senectus et netus et malesuada fames. Ut sem nulla pharetra diam sit amet. Scelerisque viverra mauris in aliquam. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus aenean. Diam maecenas sed enim ut sem viverra. Suspendisse ultrices gravida dictum fusce ut placerat orci nulla. Sed enim ut sem viverra aliquet eget. Augue lacus viverra vitae congue eu consequat.Nulla at volutpat diam ut venenatis tellus. Volutpat lacus laoreet non curabitur gravida arcu ac tortor dignissim. Euismod in pellentesque massa placerat duis ultricies. Massa massa ultricies mi quis hendrerit dolor. Condimentum id venenatis a condimentum. Vitae congue mauris rhoncus aenean vel elit. In mollis nunc sed id. Sed adipiscing diam donec adipiscing tristique risus nec feugiat. Molestie a iaculis at erat pellentesque adipiscing commodo. Diam phasellus vestibulum lorem sed. Etiam erat velit scelerisque in dictum. In cursus turpis massa tincidunt dui. Nascetur ridiculus mus mauris vitae ultricies leo integer malesuada. Nunc faucibus a pellentesque sit amet porttitor eget dolor morbi. Posuere lorem ipsum dolor sit amet consectetur. A scelerisque purus semper eget duis at. Venenatis tellus in metus vulputate eu.',
                        style: themeData.textTheme.bodyMedium,
                      ),
                    )
                  ],
                ),
                // crossAxisAlignment: CrossAxisAlignment.start,
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 116,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                    themeData.colorScheme.surface,
                    themeData.colorScheme.surface.withOpacity(0)
                  ])),
            ),
          )
        ],
      )),
    );
  }

  void showSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      behavior: SnackBarBehavior.fixed,
    ));
  }
}
